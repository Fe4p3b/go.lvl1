package main

import (
	"fmt"
	"math"
)

const (
	pi = math.Pi
)

func main() {

	var a, b int
	fmt.Printf("Enter a: ")
	fmt.Scanln(&a)

	fmt.Printf("Enter b: ")
	fmt.Scanln(&b)

	r1 := rectangleArea(a, b)
	fmt.Printf("Enter rectangle area: %d\n", r1)

	var circleArea float64
	fmt.Printf("Enter circle area: ")
	fmt.Scanln(&circleArea)

	r2 := calcDiameter(circleArea)
	fmt.Printf("Circle diameter: %f\n", r2)
	r3 := calcCircumference(circleArea)
	fmt.Printf("Circle circumference: %f\n", r3)

	var n int
	fmt.Printf("Enter 3 digit number: ")
	fmt.Scanln(&n)
	numberByDigits(n)

}

func rectangleArea(a int, b int) int {
	return a * b
}

func calcDiameter(a float64) float64 {
	d := 2 * math.Sqrt(a/pi)
	return d
}

func calcCircumference(a float64) float64 {
	c := calcDiameter(a) * pi
	return c
}

func numberByDigits(n int) {
	if n > 999 || n < 100 {
		fmt.Printf("Number is not 3 digit\n")
	} else {
		r := []int{
			n - n%100,
			n%100 - n%100%10,
			n % 100 % 10,
		}
		fmt.Printf("Number by digits: %v\n", r)
	}
}
